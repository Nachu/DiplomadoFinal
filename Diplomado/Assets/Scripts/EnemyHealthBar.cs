﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour {

    public BorderControl borderNum;
    public Image currentHealthBar;
    private Animator anim;
    private float timerDestroy = 0.0f;
    public float hitpoints = 70;
    public float maxHitPoints = 70;
    public int muertos = 0;

    void Start()
    {
        UpdateHealthBar();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("MuertoDestroy") || anim.GetCurrentAnimatorStateInfo(0).IsName("Muerto 0"))
        {
            timerDestroy += Time.deltaTime;

            if (timerDestroy >= 2)
            {
                Destroy(gameObject);
            }
        }
    }

    private void UpdateHealthBar()
    {
        float ratio = hitpoints / maxHitPoints;
        currentHealthBar.rectTransform.localScale = new Vector3(ratio, 1, 1);
    }

    private void TakeDamage(float damage)
    {
        hitpoints -= damage;
        if (hitpoints <= 0)
        {
            hitpoints = 0;
            Debug.Log("Dead alv!");
            anim.SetTrigger("PlayerDead");

        }

        UpdateHealthBar();
    }

    private void HealDamage(float heal)
    {
        hitpoints += heal;
        if (hitpoints < maxHitPoints)
            hitpoints = maxHitPoints;


        UpdateHealthBar();
    }
}
