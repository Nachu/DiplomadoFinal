﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    private Animator anim;

    private Rigidbody rgb;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    private int noOfHits;
    float lastClickedTime = 0;
    float maxComboDelay = 0.3f;
    int noOfClicks = 0;
    public bool golpe;
    public int damage = 1;
    private bool isDead = false;
    public DeathMenu deathMenu;
    public FinishMenu finishMenu;

    private void Start()
    {
        anim = GetComponent<Animator>();
        rgb = GetComponent<Rigidbody>();

        noOfHits = 0;
        golpe = false;
    }

    public void Update()
    {
        if (isDead)
            return;


        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Derecha") || anim.GetCurrentAnimatorStateInfo(0).IsName("Izquierda") || anim.GetCurrentAnimatorStateInfo(0).IsName("Patada"))
        {
            golpe = true;
        } else
        {
            golpe = false;
        }

        moveInput = new Vector3(CrossPlatformInputManager.GetAxis("Horizontal"), 0f, CrossPlatformInputManager.GetAxis("Vertical"));
        moveVelocity = moveInput * moveSpeed;

        //Animación de caminar
        if(moveVelocity.x != 0)
        {
            anim.SetBool("Walk", true);
        } else
        {
            anim.SetBool("Walk", false);
        }

        if (moveVelocity.z != 0)
        {
            anim.SetBool("Walk", true);
        }
        else
        {
            anim.SetBool("Walk", false);
        }

        //Rota izquierda y derecha
        if (moveInput.x > 0)
        {
            transform.rotation = Quaternion.Euler(0, 90, 0);
        }
        else if (moveInput.x < 0)
        {
            transform.rotation = Quaternion.Euler(0, 270, 0);
        }

        //Resetea tiempo de combo
        if (Time.time - lastClickedTime > maxComboDelay)
        {
            noOfClicks = 0;
            anim.SetBool("Attack1", false);
            anim.SetBool("Attack2", false);
            anim.SetBool("Attack3", false);
        }

        //Cuando se aprieta el boton
        if (CrossPlatformInputManager.GetButtonDown("Punch"))
        {
            noOfHits = 0;
            damage = 6;

            noOfHits++;
            lastClickedTime = Time.time;
            noOfClicks++;
            noOfClicks = Mathf.Clamp(noOfClicks, 0, 3);
            noOfHits = Mathf.Clamp(noOfHits, 0, 3);
            if (noOfClicks == 1)
            {
                anim.SetBool("Attack1", true);
            } 

            //Secuencia de combos 
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Derecha") && noOfClicks >= 2)
            {
                damage = 9;
                anim.SetBool("Attack2", true);
            }
            else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Izquierda") && noOfClicks >= 3)
            {
                damage = 11;
                anim.SetBool("Attack3", true);
            }
            else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Patada") && noOfClicks >= 3)
            {
                anim.SetBool("Attack3", false);
            }


        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Finish")
        {
            Finish();
        }

        if(other.gameObject.tag == "Potion")
        {
            GetComponent<HealthBar>().HealDamage(50f);
        }
    }

    public void Finish()
    {
        finishMenu.ToggleFinishMenu();
    }

    public void Death()
    {
        isDead = true;
        deathMenu.ToggleEndMenu();
    }

    void FixedUpdate()
    {

            rgb.velocity = moveVelocity;

    }

}
