﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroll : MonoBehaviour {

    private Transform playerTransform;
    private float cameraPos = 1.45f;
    private float cameraMove = 10.48f; //13.37f;
    private float playerRange = 5f;

    void Start () {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
	
	void Update () {
		if(playerTransform.position.x > playerRange)
        {
            playerRange += cameraMove;
            cameraPos += cameraMove;
            transform.position = new Vector3(cameraPos,transform.position.y, transform.position.z);
        }
	}
}
