﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderControl : MonoBehaviour {

    public EnemyHealthBar muertosEnemyHeal;
    private int noEnemigos = 0;
    private BoxCollider boxCol;

	void Start () {        
        boxCol =  GetComponent<BoxCollider>();
        noEnemigos = 4;
	}
	
	void Update () {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        noEnemigos = enemies.Length;

        //Debug.Log(noEnemigos);

        if(noEnemigos == 0)
        {
            boxCol.enabled = false;
        }
        else
        {
            boxCol.enabled = true;
        }
	}

}
