﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishMenu : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
    }


    public void ToggleFinishMenu()
    {
        gameObject.SetActive(true);
    }

    public void ToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
