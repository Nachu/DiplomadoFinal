﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public GameObject enemy;
    public Transform enemyPos1;
    public Transform enemyPos2;
    private float repeatRates = 5.0f;
    private int numSpawner1 = 0;
    private int numSpawner2 = 0;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
                InvokeRepeating("EnemySpawner1", 0.5f, repeatRates);
                InvokeRepeating("EnemySpawner2", 0.5f, repeatRates);

        }
    }

    void EnemySpawner1()
    {
        if(numSpawner1 < 2)
        {
            Instantiate(enemy, enemyPos1.position, enemyPos1.rotation);
            numSpawner1++;
        }        
    }

    void EnemySpawner2()
    {
        if (numSpawner2 < 2)
        {
            Instantiate(enemy, enemyPos2.position, enemyPos2.rotation);
            numSpawner2++;
        }
    }
}
