﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour {

    public Transform lookAt; //objeto que vemos // Cubo
    public Vector3 offset = new Vector3(5.0f, 1.5f, -5.0f);

    private void Start()
    {
        transform.position = lookAt.position + offset;
    }

    private void Update()
    {
        Vector3 desiredPosition = lookAt.position + offset;
        desiredPosition.x = 5;
        transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime);
        //transform.position = lookAt.position + offset;
    }
}
