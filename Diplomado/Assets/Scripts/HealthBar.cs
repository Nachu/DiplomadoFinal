﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public Image currentHealthBar;
    public Text ratioText;

    private float hitpoints = 100;
    public float maxHitPoints = 100;

	void Start () {
        UpdateHealthBar();
	}

    private void UpdateHealthBar()
    {
        float ratio = hitpoints / maxHitPoints;
        currentHealthBar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + '%';
    }

    private void TakeDamage(float damage)
    {
        hitpoints -= damage;
        if(hitpoints < 0)
        {
            hitpoints = 0;
            GetComponent<PlayerController>().Death();
        }

        UpdateHealthBar();
    }

    public void HealDamage(float heal)
    {
        hitpoints += heal;
        if (hitpoints < maxHitPoints)
            hitpoints = maxHitPoints;


        UpdateHealthBar();
    }
}
