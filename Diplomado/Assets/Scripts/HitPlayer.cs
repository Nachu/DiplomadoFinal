﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HitPlayer : MonoBehaviour {

    public EnemyMovement enemyScript;
    private int con = 1;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {            
            other.SendMessage("TakeDamage", enemyScript.damage);
        }

    }
}
