﻿using UnityEngine;

public enum Swipe { None, Up, Down, Left, Right };

public class PlayerMotor : MonoBehaviour {

    private const float VLANE_DISTANCE = 1.5f;
    private const float LANE_DISTANCE = 2.4f;

    private bool isRunning = false;

    //Movement
    private CharacterController controller;
    private float verticalVelocity;
    public float speedVertical = 5.0f;
    private int desiredLane = 1; // 0 izquierda, 1 = enmedio, 2 = derecha
    private CharacterController controllerV;
    private int desiredLaneV = 1; // 0 arriba, 1 = enmedio, 2 = abajo
    private float old_pos;

    
    //SWIPE
    public float minSwipeLength = 200f;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    public static Swipe swipeDirection;
    public float anguloSwipe = 0.5f;
    
    private void Start()
    {
        controller = GetComponent<CharacterController>();
        
    }

    private void Update()
    {
        /*
        #region Mobile Inputs
        DetectSwipe();
        #endregion

        #region Standalone Inputs
        SwipeMouse();
        #endregion
        */

        if (!isRunning)
            return;

        //Captura el input de a que linea ir HORIZONTAL
        if (MobileInput.Instance.SwipeLeft)
        {
            MoveLane(false);
            transform.rotation = Quaternion.Euler(0, 270, 0);
        }
        if (MobileInput.Instance.SwipeRight)
        {
            MoveLane(true);
            transform.rotation = Quaternion.Euler(0, 90, 0);
        }
        
        //Captura el input de a que linea ir VERTICAL
        if (MobileInput.Instance.SwipeUp)
        {
            MoveLaneV(false);
        }

        if (MobileInput.Instance.SwipeDown)
        {
            MoveLaneV(true);
        }
        
        //A que linea te puedes mover
        Vector3 targetPosition = transform.position.z * Vector3.forward;
        if (desiredLane == 0)
            targetPosition += Vector3.left * LANE_DISTANCE;
        else if (desiredLane == 2)
            targetPosition += Vector3.right * LANE_DISTANCE;

        //A que linea te puedes mover
        Vector3 targetPositionV = transform.position.z * Vector3.right;
        if (desiredLaneV == 0)
        {
            targetPositionV += Vector3.forward * VLANE_DISTANCE;
        }
        else if (desiredLaneV == 2)
        {
            targetPositionV += Vector3.back * VLANE_DISTANCE;
        }

        //Calculate move delta
        Vector3 moveVector = Vector3.zero;
        moveVector.x = (targetPosition - transform.position).normalized.x * speedVertical;
        moveVector.y = 0;
        moveVector.z = (targetPositionV - transform.position).normalized.z * speedVertical;
        //anim.SetBool("Derecha", true);

        

        //Mueve personaje 
        controller.Move(moveVector * Time.deltaTime);
    }

    private void MoveLane(bool goingLeft)
    {
        desiredLane += (goingLeft) ? 1 : -1;
        desiredLane = Mathf.Clamp(desiredLane, 0, 2);
    }

    private void MoveLaneV(bool goingDown)
    {
        desiredLaneV += (goingDown) ? 1 : -1;
        desiredLaneV = Mathf.Clamp(desiredLaneV, 0, 2);
    }

    /*
    public void DetectSwipe()
    {

        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began)
            {
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }

            if (t.phase == TouchPhase.Ended)
            {
                secondPressPos = new Vector2(t.position.x, t.position.y);
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                // Make sure it was a legit swipe, not a tap
                if (currentSwipe.magnitude < minSwipeLength)
                {
                    swipeDirection = Swipe.None;
                    return;
                }

                currentSwipe.Normalize();

                
                if (currentSwipe.y > 0 && currentSwipe.x > -anguloSwipe && currentSwipe.x <anguloSwipe) {
                    swipeDirection = Swipe.Up;
                    // Swipe up
                    //MoveLaneV(false);
                } else if (currentSwipe.y < 0 && currentSwipe.x > -anguloSwipe && currentSwipe.x < anguloSwipe) {
                    swipeDirection = Swipe.Down;
                    // Swipe down
                    //MoveLaneV(true);
                } else if (currentSwipe.x < 0 && currentSwipe.y > -anguloSwipe && currentSwipe.y < anguloSwipe) {
                    swipeDirection = Swipe.Left;
                    // Swipe left
                    //MoveLane(false);
                    transform.rotation = Quaternion.Euler(0, 270, 0);
                } else if (currentSwipe.x > 0 && currentSwipe.y > -anguloSwipe && currentSwipe.y < anguloSwipe) {
                    swipeDirection = Swipe.Right;
                    // Swipe right
                    //MoveLane(true);
                    transform.rotation = Quaternion.Euler(0, 90, 0);
                }
            }
        }
        else
        {
            swipeDirection = Swipe.None;
        }
    }


    public void SwipeMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //save began touch 2d point
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            //save ended touch 2d point
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //create vector from the two points
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            //normalize the 2d vector
            currentSwipe.Normalize();

            //swipe up
            if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
                Debug.Log("up swipe");
                MoveLaneV(false);
            }
            //swipe down
            if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
                Debug.Log("down swipe");
                MoveLaneV(true);
            }
            //swipe left
            if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
                Debug.Log("left swipe");
                MoveLane(false);
                transform.rotation = Quaternion.Euler(0, 270, 0);
            }
            //swipe right
            if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
                Debug.Log("right swipe");
                MoveLane(true);
                transform.rotation = Quaternion.Euler(0, 90, 0);
            }
        }
    }
    */
    
    public void StartRunning()
    {
        isRunning = true;
    }
}


