﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {

    public EnemyHealthBar vida;
    Transform player;
    NavMeshAgent nav;
    private Animator anim;
    public GameObject Enemy;
    public AudioClip audioDaño;
    private float Distance;
    public PlayerController playerScript;
    public bool Golpe = false;
    private bool der, iz, pa = false;
    private float timer = 0.0f;
    //private float timerDestroy = 0.0f;
    private int seconds;
    public float damage = 10;
    public bool golpe = false;


    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        anim = GetComponent<Animator>();
        nav = GetComponent<NavMeshAgent>();
        AudioSource audioDaño = GetComponent<AudioSource>();
        
    }


    void Update()
    {

        Distance = Vector3.Distance(player.transform.position, transform.position);
        if (Distance >= 2 && vida.hitpoints != 0)
        {

            anim.SetBool("Camina", true);
            nav.SetDestination(player.position);
            timer = 0.0f;
            golpe = false;
        }
        else
        {
            anim.SetBool("Camina", false);
            timer += Time.deltaTime;
            if (timer > 3.0f)
            {
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                {
                    anim.SetTrigger("Golpe");
                    timer = 0.0f;
                    Golpe = true;
                    golpe = true;

                }
                
            }
            golpe = false;
        }

        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Golpe"))
        {
            Golpe = false;
        }
	}


    private void OnTriggerEnter(Collider other)
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = audioDaño;

        //Debug.Log(gameObject.name + " tocó a " + other.gameObject.name);
        der = false;
        iz = false;
        pa = false;

        if(other.gameObject.name == "PuñoDe"  && playerScript.golpe == true && der == false)
        {
            anim.SetTrigger("daño1");
            audio.Play();
            //Debug.Log("golpe1");
            der = true;
            timer = 0.0f;
        } else if (other.gameObject.name == "PuñoIz" && playerScript.golpe == true && iz == false)
        {
            anim.SetTrigger("daño2");
            audio.Play();
            //Debug.Log("golpe2");
            iz = true;
            timer = 0.0f;
        } else if (other.gameObject.name == "PieD" && playerScript.golpe == true && pa == false)
        {
            anim.SetTrigger("muerto1");
            audio.Play();
            //Debug.Log(vida);
            //Debug.Log("patada");
            timer = 0.0f;
        }

        
    }
}
