﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MapManager : MonoBehaviour {

    public GameObject[] mapPrefab;

    private Transform playerTransform;
    private float spawnX = 11.93f;
    private float mapLength = 10.48f;
    private float safeZone = 0;
    private int mapsOnScreen = 2;
    private int lastPrefabIndex = 0;
    private int mapaRandom;
    private int numMapas;
    private int noEnemigos = 0;

    private List<GameObject> activeMaps;

	
	void Start () {
        //GameObject map1;
        activeMaps = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        SpawnMap();
        SpawnMap();
        //Debug.Log(mapPrefab.Length);
        GameObject  map1 = Instantiate(mapPrefab[0]) as GameObject;
        numMapas = 2;
    }
	
	void Update () {
        if (playerTransform.position.x - safeZone > (spawnX - mapsOnScreen * mapLength))
        {
            SpawnMap();
            

            if (activeMaps.Count >= 5)
            {
                DeleteMap();
            }
        }

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        noEnemigos = enemies.Length;

        if(numMapas == 10 && noEnemigos == 0)
        {
            Debug.Log("Mapa 10");
        }
    }

    private void SpawnMap(int prefabIndex = -1)
    {

        numMapas++;
        //Debug.Log(numMapas);
        GameObject go;

        if(numMapas < 5)
        {
            go = Instantiate(mapPrefab[RandomPrefabIndex()]) as GameObject;
        } else
        {
            go = Instantiate(mapPrefab[1]) as GameObject;
        }
        
        go.transform.SetParent (transform);
        go.transform.position = Vector3.right * spawnX;
        spawnX += mapLength;
        activeMaps.Add(go);
        
}

    private void DeleteMap()
    {
        Destroy(activeMaps[0]);
        activeMaps.RemoveAt(0);
    }

    private int RandomPrefabIndex()
    {
        if (mapPrefab.Length <= 1)
            return 0;

        int randomIndex = lastPrefabIndex;
        while(randomIndex == lastPrefabIndex)
        {
            randomIndex = Random.Range(2, mapPrefab.Length);
        }

        lastPrefabIndex = randomIndex;
        return randomIndex;
    }
}
