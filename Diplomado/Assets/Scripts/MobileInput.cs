﻿using UnityEngine;
using System.Collections;

public class MobileInput : MonoBehaviour {

    private const float DEADZONE = 100.0f;

    public static MobileInput Instance { set; get; }

    private Animator anim;

    private bool tap, swipeLeft, swipeRight, swipeUp, swipeDown;
    private Vector2 swipeDelta, startTouch;

    public bool Tap { get { return tap; } }
    public Vector2 SwipeDelta { get { return swipeDelta; } }
    public bool SwipeLeft { get { return swipeLeft; } }
    public bool SwipeRight { get { return swipeRight; } }
    public bool SwipeUp { get { return swipeUp; } }
    public bool SwipeDown { get { return swipeDown; } }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        //Resetea booleanons
        tap = swipeDown = swipeLeft = swipeRight = swipeUp = false;

        //Checa input
        #region StandAlone Inputs
        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            startTouch = Input.mousePosition;
            
        } else if (Input.GetMouseButtonUp(0))
        {
            startTouch = swipeDelta = Vector2.zero;
        }
        #endregion

        #region Mobile Inputs
        if (Input.touches.Length !=0)
        {
            if(Input.touches[0].phase == TouchPhase.Began)
            {
                tap = true;
                startTouch = Input.mousePosition;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                startTouch = swipeDelta = Vector2.zero;

            }
        }

        #endregion

        //Calcula distancia de touch
        swipeDelta = Vector2.zero;
        if(startTouch != Vector2.zero)
        {
            //Checa mobile
            if(Input.touches.Length != 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;
            }
            //Checa standalone
            else if (Input.GetMouseButton(0))
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        //Checa que el rango minimo del swipe
        if (swipeDelta.magnitude > DEADZONE)
        {
            //Swipe confirmado
            float x = swipeDelta.x;
            float y = swipeDelta.y;

            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                //Izquierda o Derecha
                if (x < 0)
                {
                    swipeLeft = true;

                }
                else
                {
                    swipeRight = true;
                }
            }
            else
            {
                //Arriba o abajo
                if (y < 0)
                {
                    swipeDown = true;
                }
                else
                {
                    swipeUp = true;
                }
            }
            startTouch = swipeDelta = Vector2.zero;
        } else if (Input.GetMouseButtonDown(0))
        {
            Ataque();
        }

    }

    private void Ataque()
    {
        anim.SetTrigger("Derecha");
    }
}
