﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuInGame : MonoBehaviour
{
    public GameObject menu;

    public void Start ()
    {
        menu.SetActive(false);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        menu.SetActive(true);
    }
    public void ContinueGame()
    {
        Time.timeScale = 1;
        menu.SetActive(false);
    }
}
