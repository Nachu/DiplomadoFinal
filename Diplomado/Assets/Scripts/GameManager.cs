﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager Instance { set; get; }

    private bool isGamestarted = false;
    private PlayerMotor motor;

    //UI
    public Text lifesText, comboText, scoreText;
    private float lifeScore, comboScore, score;

    private void Awake()
    {
        Instance = this;
        //UpdateScores();
        motor = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMotor>();
    }

    private void Update()
    {
        if(MobileInput.Instance.Tap && !isGamestarted)
        {
            isGamestarted = true;
            motor.StartRunning();
        }
    }

    public void UpdateScores()
    {
        scoreText.text = score.ToString();
        lifesText.text = lifeScore.ToString();
        comboText.text = comboScore.ToString();
    }
}
