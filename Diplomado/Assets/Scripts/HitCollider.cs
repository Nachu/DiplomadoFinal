﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCollider : MonoBehaviour {

    public PlayerController playerScript;
    private int con = 1;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && playerScript.golpe == true)
        {
            other.SendMessage("TakeDamage", playerScript.damage);
            con++;

        }
    }
}
